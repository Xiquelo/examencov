#Ingresar un texto y decir si es string, numero o alfanumerico

# Declaracion de variables
txt = input('Ingresa tu texto: ')
part = txt.partition( '.' )
part2 = txt.split()

# Condicion string
if txt.isdigit():
    print( 'Es una cadena de digitos' )

# Valida 3 posibilidades ( 0.0 / .0 / 0. )
elif (part[0].isdigit() and part[1] == '.' and part[2].isdigit()) or (part[0] == '' and part[1] == '.' and part[2].isdigit()) or (part[0].isdigit() and part[1] == '.' and part[2] == ''):
    neotxt = float( txt )
    print(neotxt)
    print( 'Es una cadena de digitos' )

# elif (part2[0].isalnum() and part2[1] == ' ' and part2[2].isalnum()) or (part2[0] == '' and part2[1] == ' ' and part2[2].isalnum()) or (part2[0].isalnum() and part2[1] == ' ' and part2[2] == ''):
elif txt.isalnum():
    print( 'Es una cadena de caracteres y numeros' )

else:
    print( 'Es una cadena de texto' )
