# Lista de una frase separada por espacios y contar las veces que aparece mundo

frase = input( 'Ingresa tu frase: ' )
fraseL = frase.lower()

# Cuenta el no. de veces de la palabra mundo
conteo = fraseL.count( 'mundo' )

# Divide la frase en palabras individuales
part = frase.split()

# Se crea lista vacia
lista_mundo = []

# Se crea acumulador
n = 0

# Agregando palabras hasta que no haya nada que agregar
for i in part:
    lista_mundo.append( part[n] ) 
    n += 1
    
# Imprime cada palabra de la lista_mundo
for word in lista_mundo:
    print( word )

print( 'Mundo aparece un total de:',conteo, 'veces' )
