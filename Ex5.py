# Ingresar caracter e indicar si es letra(mayuscula/minuscula), numero o ninguno

carac = input( 'Ingresa tu caracter: ' )

if carac.isupper():
    print( 'Es una mayuscula' )
elif carac.islower():
    print( 'Es una minuscula' )
elif carac.isdigit():
    print( 'Es un numero' )
else:
    print( 'No es ni letra ni numero' )
