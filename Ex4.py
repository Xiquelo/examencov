# Lista de pares e impares

num = int( input( 'Ingresa tu numero: ' ) )

list_par = []
list_non = []

n = 1

# Condicion donde si el residuo es 0 se trata de un par
while n <= num:
    if n % 2 == 0:
        list_par.append( n )
    else:
        list_non.append( n )
    n += 1
else:
    print( 'Lista Pares' )
    print( list_par )
    print( 'Lista Nones' )
    print( list_non )
